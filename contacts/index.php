<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

    <section class="section section_no-top-padding contacts-details">
      <div class="container">
        <h2>ОСНОВНОЙ ОФИС</h2>
        <div class="contacts-details__list text-lg row">
          <div class="col-xs-12 col-sm-6">
            <div class="contacts-details__item">
              <i class="contacts-details__icon icon-geo"></i>
              г. Грозный, ул. Субры Кишиевой (бывшая ул. Киевская), 9
            </div>
            <div class="contacts-details__item">
              <i class="contacts-details__icon icon-phone"></i>
              <a href="tel:+78005004671" class="tel">+7 (800) 500-46-71</a>
            </div>
            <div class="contacts-details__item">
              <i class="contacts-details__icon icon-mail"></i>
              <a href="mailto:info@cmo-95.ru">info@cmo-95.ru</a> (для общих вопросов)
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="contacts-details__item">
              <i class="contacts-details__icon icon-clock"></i>
              Понедельник — Пятница с 10:00 до 19:00<br>
              Cуббота c 11:00 до 18:00<br>
              Воскресенье выходной
            </div>
          </div>
        </div>
      </div>
    </section>

    <div class="map" id="map"></div>

	<?$APPLICATION->IncludeComponent("bitrix:news.list", "contact_divisions", Array(
	"COMPONENT_TEMPLATE" => "job_vacancy",
		"IBLOCK_TYPE" => "contact_divisions",	// Тип информационного блока (используется только для проверки)
		"IBLOCK_ID" => "8",	// Код информационного блока
		"NEWS_COUNT" => "20",	// Количество новостей на странице
		"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
		"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
		"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"FILTER_NAME" => "",	// Фильтр
		"FIELD_CODE" => array(	// Поля
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
			2 => "",
			3 => "",
			4 => "",
		),
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		"DISPLAY_DATE" => "N",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SHOW_404" => "N",	// Показ специальной страницы
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
	),
	false
);?>

<section class="section section_no-top-padding details">
	<div class="container">
		<h2>РЕКВИЗИТЫ</h2>
		<p>
			ОГРН	1107746955524<br>
			ИНН / КПП	7735573603 / 773501001<br>
			Полное наименование	Общество с ограниченной ответственностью «МПО»<br>
			Сокращенное наименование	ООО «МПО»<br>
			Юр. адрес	124498, Москва г, Зеленоград г, 4922-й проезд, дом № 4, строение 5<br>
			Почтовый адрес	124498, Грозный, 4922-й проезд, дом № 4, строение 5<br>
			Фактический адрес	Москва, Москва, Улица Орджоникидзе, дом 11, строение 10 (метро Ленинский проспект)<br>
			Москва, улица Шаболовка, дом 34, строение 6 (метро Шаболовская)<br>
			Грозный, проезд 4922-й, дом 4, строение 5 (Технопарк «ЭЛМА»)
		</p>
		<div class="details__action">
			<a href="" class="button button_primary">Скачать</a>
		</div>
	</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>