<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новый раздел");
?><div class="container">
 <article class="article"> <img src="/pictures/img.jpg" alt="" class="img-right">
	<p class="text-compact">
		 МУП «Центр Муниципального Заказа г. Грозного» учрежден мэрией г. Грозного с целью обеспечения населения города и республики качественными оригинальными лекарственными средствами, изделиями медицинского назначения, а также, товарами широкого потребления. Главная особенность предприятия состоит в том, что его основная цель — не извлечение прибыли, а своевременная и бесперебойная поставка качественных и доступных товаров и услуг населению. Компания сертифицирована по международной системе менеджмента качества ИСО 9001, что подтверждает высокий уровень менеджмента на предприятии.
	</p>
	<hr>
 <section class="features">
	<h2>ОСНОВНЫЕ НАПРАВЛЕНИЯ ДЕЯТЕЛЬНОСТИ</h2>
	<div class="features__list row">
		<div class="features__item col-xs-12 col-sm-6 col-md-4">
			<div class="features__content">
				<div class="features__icon-placeholder">
 <i class="features__icon icon-certificate"></i>
				</div>
				<p class="features__text">
					 обеспечение населения качественными товарами и услугами во всех сферах на льготных условиях
				</p>
			</div>
		</div>
		<div class="features__item col-xs-12 col-sm-6 col-md-4">
			<div class="features__content">
				<div class="features__icon-placeholder">
 <i class="features__icon icon-house"></i>
				</div>
				<p class="features__text">
					 участие в реализации городских программ обеспечения населения гарантированным уровнем льготной помощи различными видами товаров и услуг
				</p>
			</div>
		</div>
		<div class="features__item col-xs-12 col-sm-6 col-md-4">
			<div class="features__content">
				<div class="features__icon-placeholder">
 <i class="features__icon icon-list"></i>
				</div>
				<p class="features__text">
					 разработка общегородских нормативов и стандартов в области обеспечения населения всеми типами товаров и услуг
				</p>
			</div>
		</div>
		<div class="features__item col-xs-12 col-sm-6 col-md-4">
			<div class="features__content">
				<div class="features__icon-placeholder">
 <i class="features__icon icon-earth"></i>
				</div>
				<p class="features__text">
					 участие в развитии внешнеэкономических связей международного, межрегионального и межведомственного сотрудничества, направленных на удовлетворение потребностей населения
				</p>
			</div>
		</div>
		<div class="features__item col-xs-12 col-sm-6 col-md-4">
			<div class="features__content">
				<div class="features__icon-placeholder">
 <i class="features__icon icon-family"></i>
				</div>
				<p class="features__text">
					 участие в разработке и реализации программ социально-экономического развития и создании социально-ориентированной системы помощи всем категориям населения города
				</p>
			</div>
		</div>
		<div class="features__item col-xs-12 col-sm-6 col-md-4">
			<div class="features__content">
				<div class="features__icon-placeholder">
 <i class="features__icon icon-cart"></i>
				</div>
				<p class="features__text">
					 разработка списков товаров и услуг для льготного обеспечения населения города, совершенствование системы их адресного предоставления на льготных условиях
				</p>
			</div>
		</div>
	</div>
 </section>
	<hr>
	<p class="text-center">
		 МУП «Центр Муниципального Заказа г. Грозного» учрежден мэрией г. Грозного с целью обеспечения населения города и республики качественными оригинальными лекарственными средствами, изделиями медицинского назначения, а также, товарами широкого потребления. Главная особенность предприятия состоит в том, что его основная цель - не извлечение прибыли, а своевременная и бесперебойная поставка качественных и доступных товаров и услуг населению. Компания сертифицирована по международной системе менеджмента качества ИСО 9001, что подтверждает высокий уровень менеджмента на предприятии.
	</p>
 <section class="certificates">
	<h2>ЛИЦЕНЗИИ И СЕРТИФИКАТЫ</h2>
	<div class="certificates__list row">
		<div class="certificates__item col-xs-12 col-sm-6 col-md-3">
 <a href="/pictures/certificates/1.jpg" class="certificates__img-placeholder" title="Лицензия на формацевтическую деятельность 2014" data-gallery=""> <img src="/pictures/certificates/1.jpg" alt="" class="certificates__img"> </a>
			<div class="certificates__text">
				 Лицензия на формацевтическую деятельность 2014
			</div>
		</div>
		<div class="certificates__item col-xs-12 col-sm-6 col-md-3">
 <a href="/pictures/certificates/1.jpg" class="certificates__img-placeholder" title="Лицензия на формацевтическую деятельность 2015" data-gallery=""> <img src="/pictures/certificates/1.jpg" alt="" class="certificates__img"> </a>
			<div class="certificates__text">
				 Лицензия на формацевтическую деятельность 2015
			</div>
		</div>
		<div class="certificates__item col-xs-12 col-sm-6 col-md-3">
 <a href="/pictures/certificates/1.jpg" class="certificates__img-placeholder" title="Лицензия на формацевтическую деятельность 2016" data-gallery=""> <img src="/pictures/certificates/1.jpg" alt="" class="certificates__img"> </a>
			<div class="certificates__text">
				 Лицензия на формацевтическую деятельность 2016
			</div>
		</div>
		<div class="certificates__item col-xs-12 col-sm-6 col-md-3">
 <a href="/pictures/certificates/1.jpg" class="certificates__img-placeholder" title="Лицензия на формацевтическую деятельность 2017" data-gallery=""> <img src="/pictures/certificates/1.jpg" alt="" class="certificates__img"> </a>
			<div class="certificates__text">
				 Лицензия на формацевтическую деятельность 2017
			</div>
		</div>
	</div>
 </section> </article>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>