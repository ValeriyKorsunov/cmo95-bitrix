<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
?> 

	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list", 
		"slider", 
		array(
			"COMPONENT_TEMPLATE" => "slider",
			"IBLOCK_TYPE" => "MainP",
			"IBLOCK_ID" => "1",
			"NEWS_COUNT" => "10",
			"SORT_BY1" => "SORT",
			"SORT_ORDER1" => "ASC",
			"SORT_BY2" => "TIMESTAMP_X",
			"SORT_ORDER2" => "ASC",
			"FILTER_NAME" => "",
			"FIELD_CODE" => array(
				0 => "NAME",
				1 => "PREVIEW_PICTURE",
				2 => "",
			),
			"PROPERTY_CODE" => array(
				0 => "name_button",
				1 => "url_button",
				2 => "",
			),
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.M.Y",
			"SET_TITLE" => "N",
			"SET_BROWSER_TITLE" => "N",
			"SET_META_KEYWORDS" => "N",
			"SET_META_DESCRIPTION" => "N",
			"SET_LAST_MODIFIED" => "N",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"ADD_SECTIONS_CHAIN" => "N",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"INCLUDE_SUBSECTIONS" => "N",
			"STRICT_SECTION_CHECK" => "N",
			"DISPLAY_DATE" => "N",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"PAGER_TEMPLATE" => ".default",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"PAGER_TITLE" => "Новости",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"SET_STATUS_404" => "N",
			"SHOW_404" => "N",
			"MESSAGE_404" => ""
			),
			false
		);
	?>

    <section class="section section_text">
      <div class="container container_lg">
        <div class="section__content">
          <h2 class="section__title">Наша миссия</h2>
          <p>Муниципальное унитарное предприятие "Центр муниципального заказа г. Грозного" учреждено мэрией г. Грозного с целью обеспечения населения города и республики качественными оригинальными лекарственными средствами, изделиями медицинского назначения, а также, товарами широкого потребления. Главная особенность предприятия состоит в том, что его основная цель — не извлечение прибыли, а своевременная и бесперебойная поставка качественных и доступных товаров и услуг населению.</p>
        </div>
        <hr>
      </div>
      <div class="container">
        <section class="features features_sm">
          <h2 class="section__title">ЦЕЛЬ ОРГАНИЗАЦИИ</h2>
          <div class="features__list row">
            <div class="features__item col-xs-12 col-sm-6 col-md-4">
              <div class="features__content">
                <div class="features__icon-placeholder">
                  <i class="features__icon icon-money"></i>
                </div>
                <p class="features__text">Контроль уровня цен в республике по всем социально значимым группам товаров</p>
              </div>
            </div>
            <div class="features__item col-xs-12 col-sm-6 col-md-4">
              <div class="features__content">
                <div class="features__icon-placeholder">
                  <i class="features__icon icon-handshake"></i>
                </div>
                <p class="features__text">Контроль за качеством товара путем заключения прямых контрактов с производителями</p>
              </div>
            </div>
            <div class="features__item col-xs-12 col-sm-6 col-md-4">
              <div class="features__content">
                <div class="features__icon-placeholder">
                  <i class="features__icon icon-cart"></i>
                </div>
                <p class="features__text">Поддержание необходимого уровня товарного запаса по социально значимым продуктам</p>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>

    <section class="section section_text section_bg-img-dark promo">
		<div class="container">
			<h2 class="promo__title section__title">Качественные  лекарства по&nbsp;доступным ценам онлайн</h2>
			<p class="promo__text">выдача лекарственных препаратов и изделий медицинского назначения, а также с 50% оплатой их стоимости льготным категориям граждан Московской области (в т.ч. детям до 3-х лет, детям до 6-ти лет из многодетных семей и беременным женщинам)</p>
			<div class="promo__action">
			<a href="" class="promo__button button button_primary">Перейти в аптеку</a>
			</div>
			<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"slider_partners", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "MainP",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "logo",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "TIMESTAMP_X",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "slider_partners"
	),
	false
);?>
		</div>
    </section>

    <section class="section section_text">
      <div class="container">
        <h2 class="section__title">НАЙДИТЕ АПТЕКУ РЯДОМ С ВАМИ</h2>
        <p>Выдача лекарственных препаратов и изделий медицинского назначения, а также с 50% оплатой их стоимости льготным категориям граждан Московской области (в т.ч. детям до 3-х лет, детям до 6-ти лет из многодетных семей и беременным женщинам)</p>
      </div>
    </section>

    <div class="map" id="map"></div>

    <section class="section section_text">
      <div class="container">
        <h2 class="section__title">НАШИ КОНТАКТЫ</h2>
        <p>Вы можете связаться с нами любым из представленных способов. Наши менеджеры ответят на любые ваши вопросы</p>
        <div class="text-lg">
          <p>
            г. Грозный, ул. Субры Кишиевой <br>
            (бывшая ул. Киевская), 9<br>
            <a href="tel:+78005004671" class="tel">+7 (800) 500-46-71</a> / <a href="mailto:info@cmo-95.ru">info@cmo-95.ru</a>
          </p>
        </div>
      </div>
    </section>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>