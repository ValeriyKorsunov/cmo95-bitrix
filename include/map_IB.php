<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$response = array();

$res = CIBlockElement::GetList(Array("SORT"=>"SORT"), Array("IBLOCK_CODE"=>'Pharmacies'), false, Array(), Array());
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();

    $properties = CIBlockElement::GetProperty(
        $arFields['IBLOCK_ID'],
        $arFields['ID'],
        Array(),
        Array('CODE '=>'coordinates')
    );
    $ar_props = $properties->Fetch();

    $arr_value = explode(',',$ar_props['VALUE']);

    $response[] = array(
        'NAME'=> $arFields['NAME'],
        'VALUE0'=> "$arr_value[0]",
        'VALUE1'=> "$arr_value[1]",
		'PREVIEW_TEXT'=>$arFields['PREVIEW_TEXT']
	);
}

echo json_encode($response,utf-8);

//echo '<pre>';
//var_dump($_POST);
//echo '</pre>';
