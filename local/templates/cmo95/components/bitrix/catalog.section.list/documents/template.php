<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $ararSectionList */
$this->setFrameMode(true);

?>

<div class="container">
	<article class="article">
		<?foreach ($arResult as $section):?>
		<?if($section["ELEMENTS"]):?>
			<h2>
				<?echo $section["NAME"];?>
			</h2>
			<div class="article__docs docs">
				<ul class="docs__list">
				<?foreach ($section["ELEMENTS"] as $element):?>
					<li class="docs__item">
						<?if($element["FILE_EXTENSION"]=="doc" || $element["FILE_EXTENSION"]=="docx"):?>
							<i class="docs__icon docs__icon_pdf icon-doc"></i>
						<?elseif($element["FILE_EXTENSION"]=="xls" || $element["FILE_EXTENSION"]=="xlsx"):?>
							<i class="docs__icon docs__icon_doc icon-xls"></i>
						<?else:?>
							<i class="docs__icon docs__icon_doc icon-file"></i>
						<?endif;?>
						<a href="<?echo $element["FILE"]?>" target="_blank" class="docs__link"><?=$element["NAME"]?></a>
					</li>
				<?endforeach;?>
				</ul>
			</div>
		<?endif;?>
		<?endforeach;?>
	</article>
</div>

<? echo ""; ?>