<?
// все разделы
$arSectionList = array();
$items = GetIBlockSectionList($arParams["IBLOCK_ID"]);
$i=0;
while($arItem = $items->GetNext())
{
	$arSectionList[$i]["NAME"] = $arItem["NAME"];
	$arElementList = CIBlockElement::GetList(
		Array(), 
		Array(
			"IBLOCK_ID" =>$arParams["IBLOCK_ID"],
			"SECTION_ID" => $arItem["ID"],
			"ACTIVE" => "Y"
		), 
		false, 
		false, 
		Array(
			'ID', 
			'NAME',
			'IBLOCK_ID',
			'PROPERTY_FILE',)
		);
	$j = 0;
	while($rs = $arElementList->Fetch())
	{
		$arSectionList[$i]["ELEMENTS"][$j]["ID_ELEMENT"] = $rs["ID"];
		$arSectionList[$i]["ELEMENTS"][$j]["NAME"] = $rs["NAME"];
		$rsFile = CFile::GetPath($rs["PROPERTY_FILE_VALUE"]);
		$arSectionList[$i]["ELEMENTS"][$j]["FILE"] = $rsFile;
		$arSectionList[$i]["ELEMENTS"][$j]["FILE_EXTENSION"] = substr(strrchr($rsFile, '.'), 1);
		$j++;
	}

	$i++;
}

$arResult = $arSectionList;

?>