<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<nav class="header__nav nav" data-nav>
	<ul class="nav__list">
	<?
	$previousLevel = 0;
	foreach($arResult as $arItem):?>

		<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
			<?=str_repeat("</ul></div></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
		<?endif?>

		<?if ($arItem["IS_PARENT"]):?>
			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li class="nav__item nav__item_with-menu" data-nav-item>
				<span class="nav__link" data-subnav-toggle>
					<?=$arItem["TEXT"]?>
					<i class="nav__icon icon-caret-down"></i>
				</span>
				<div class="nav__subnav subnav" data-subnav>
					<ul class="subnav__list">
			<?endif?>
		<?else:?>

			<?if ($arItem["PERMISSION"] > "D"):?>

				<?if ($arItem["DEPTH_LEVEL"] == 1):?>
					<li><a 
						href="<?=$arItem["LINK"]?>" 
						class="nav__link <?if ($arItem["SELECTED"]):?> active <?endif;?>">
							<?=$arItem["TEXT"]?>
						</a>
					</li>
				<?else:?>
					<li class="subnav__item <?if ($arItem["SELECTED"]):?> active <?endif?>">
						<a href="<?=$arItem["LINK"]?>" class="subnav__link" >
							<?=$arItem["TEXT"]?>
						</a>
					</li>
				<?endif?>
			<?else:?>

				<?if ($arItem["DEPTH_LEVEL"] == 1):?>
					<li><a href="" class="nav__link <?if ($arItem["SELECTED"]):?> active <?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
				<?else:?>
					<li><a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
				<?endif?>

			<?endif?>

		<?endif?>

		<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

	<?endforeach?>

	<?if ($previousLevel > 1)://close last item tags?>
		<?=str_repeat("</ul></div></li>", ($previousLevel-1) );?>
	<?endif?>

		<li class="nav__item nav__item_button">
			<a href="" class="nav__button button button_primary">
			Онлайн Аптека
			</a>
		</li>
	</ul>
</nav>
<?endif?>

