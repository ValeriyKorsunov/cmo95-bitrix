<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="container">
    <nav class="footer__nav nav">
		<ul class="nav__list">
		<?foreach($arResult as $arItem):?>
			<li class="nav__item">
				<a href="<?=$arItem["LINK"]?>" class="nav__link">
					<?=$arItem["TEXT"]?>
				</a>
			</li>
		<?endforeach?>
			<li class="nav__item nav__item_button">
				<a href="" class="nav__button button button_primary">
					Онлайн Аптека
				</a>
			</li>
		</ul>
	</nav>
</div>
<?endif?>
