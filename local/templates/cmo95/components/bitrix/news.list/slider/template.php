<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="hero" data-hero-slider>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="hero__item section_bg-img" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="hero__content">
			<div class="hero__container container">
				<h2 class="hero__title h1"><?echo $arItem["NAME"]?></h2>
				<a 
				href="<?=$arItem["DISPLAY_PROPERTIES"]["url_button"]["DISPLAY_VALUE"]?>" 
				class="hero__button button button_default">
					<?=$arItem["DISPLAY_PROPERTIES"]["name_button"]["DISPLAY_VALUE"]?>
				</a>
			</div>
        </div>
    </div>

<?endforeach;?>
</section>