<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrilogo_idComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrilogo_idComponent $component */
$this->setFrameMode(true);
?>
<?if (!empty($arResult["ITEMS"])):?>
<div class="promo__brands brands">
	<div class="brands__list" data-brands-slider>
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="brands__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<? $logo_id = $arItem["DISPLAY_PROPERTIES"]["logo"]["VALUE"]?>
			<? $logo_src = CFile::GetPath($logo_id); ?>
			<img src="<?=$logo_src?>" alt="" class="brands__img">
		</div>
	<?endforeach;?>
	</div>
</div>
<?endif;?>