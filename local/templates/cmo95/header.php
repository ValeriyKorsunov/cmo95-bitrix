<?
	if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
		die();
?>
<?
	define("IS_INDEX_PAGE",$APPLICATION->GetCurPage(true) == SITE_DIR."index.php");
	define("IS_JOBS_PAGE",$APPLICATION->GetCurPage(true) == SITE_DIR."jobs/index.php");
	IS_JOBS_PAGE;
?>
<!DOCTYPE html>
<html>
<head>
	<?$APPLICATION->ShowHead();?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?$APPLICATION->ShowTitle(false);?></title>

	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" /> 

	<link href="<?=SITE_TEMPLATE_PATH?>/css/main.css" rel="stylesheet">
	<link href="<?=SITE_TEMPLATE_PATH?>/js/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
</head>

<body>
	<div id="panel">
		<?$APPLICATION->ShowPanel();?>
	</div>
	<header class="header header_no-border" data-header>
		<div class="header__container container">
		<div class="header__logo">
			<a href="/" class="header__logo-link">
			<img src="<?=SITE_TEMPLATE_PATH?>/i/logo.png" srcset="<?=SITE_TEMPLATE_PATH?>/i/logo@2x.png 2x" alt="Муниципальное унитарное предприятние «Центр муниципального заказа г. Грозного»" class="header__logo-img" width="138" height="123">
			</a>
		</div>
		<div class="header__content">
			<div class="header__top">
			<div class="header__title">
				<?$APPLICATION->IncludeFile(SITE_DIR."/include/header_title.php", array(), array(MODE => "html")); ?>
			</div>
			<div class="header__contacts contacts-info" data-contacts>
				<?$APPLICATION->IncludeFile(SITE_DIR."/include/phone_mail.php", array(), array(MODE => "html")); ?>	
			</div>
			<div class="header__mobile-nav">
				<a href="#" class="header__mobile-button icon-phone" title="Контакты" data-contacts-toggle></a>
				<a href="#" class="header__mobile-button icon-menu" title="Меню" data-nav-toggle></a>
			</div>
			</div>
			<?$APPLICATION->IncludeComponent("bitrix:menu", "menu", Array(
				"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
					"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
					"DELAY" => "N",	// Откладывать выполнение шаблона меню
					"MAX_LEVEL" => "2",	// Уровень вложенности меню
					"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
					"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
					"MENU_CACHE_TYPE" => "N",	// Тип кеширования
					"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
					"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
					"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
					"COMPONENT_TEMPLATE" => "horizontal_multilevel"
				),
				false
			);?>
		</div>
		</div>
  	</header><!-- /.header -->

	<main class="content">
		<?if(!IS_INDEX_PAGE):?>
			<div class="page-header">
				<div class="page-header__container container">
					<h1><?$APPLICATION->ShowTitle();?></h1>
					<?$APPLICATION->IncludeComponent(
						"bitrix:breadcrumb", 
						"breadcrumb", 
						array(
							"PATH" => "",
							"SITE_ID" => "s1",
							"START_FROM" => "0",
							"COMPONENT_TEMPLATE" => "breadcrumb"
						),
						false
					);?>
				</div>
			</div>
		<?endif;?>


		
						