ymaps.ready(function () {
	var jsonAdres = [];
	$.ajax({
		type : 'POST',
		dataType: 'json',
		async:false,
		url : '/include/map_IB.php',
		data : {djson:true},
		success : function (djson){
			jsonAdres = djson;
		}
	});
	var myMap = new ymaps.Map('map', {
		center: [43.317007, 45.694693],
		controls: ["zoomControl"],
		zoom:15
	});

	for(var i in jsonAdres){
		myMap.geoObjects.add(
			new ymaps.Placemark(
				[jsonAdres[i].VALUE0, jsonAdres[i].VALUE1], 
				{
					hintContent: jsonAdres[i].NAME,
					balloonContent: '<div class="popover">' + jsonAdres[i].PREVIEW_TEXT + '</div>'
				}, 
				{
					iconLayout: 'default#image',
					iconImageHref: '/local/templates/cmo95/i/geo.svg',
					iconImageSize: [27, 40],
					iconImageOffset: [-13, -38],
					balloonMaxWidth: 300,
					balloonOffset: [120,100],
					hideIconOnBalloonOpen: false
				}
			)
		);
	}

	myMap.behaviors.disable('scrollZoom');
});