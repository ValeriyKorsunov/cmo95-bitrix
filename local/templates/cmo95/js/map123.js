ymaps.ready(function () {
	var myMap = new ymaps.Map('map', {
		center: [43.317007, 45.694693],
		controls: ["zoomControl"],
		zoom:15
	});

	var jsonAdres = [];
	$.ajax({
		type : 'POST',
		dataType: 'json',
		async:false,
		url : '/include/map_IB.php',
		data : {djson:true},
		success : function (djson){
			alert('***');
			jsonAdres = djson;
		}
	});

  myPlacemark1 = new ymaps.Placemark([43.317685, 45.693855], {
    hintContent: 'Аптека 1',
    balloonContent: '<div class="popover"><p>Телефон: <a href="tel:+78005004671" class="tel">+7 (800) 500-46-71</a></p>' +
                    '<p>Адрес: г. Грозный, ул. Субры Кишиевой (бывшая ул. Киевская), 9</p>' + 
                    '<p>E-mail: <a href="mailto:info@cmo-95.ru">info@cmo-95.ru</a></p></div>'
  }, {
    iconLayout: 'default#image',
    iconImageHref: '/local/templates/cmo95/i/geo.svg',
    iconImageSize: [27, 40],
    iconImageOffset: [-13, -38],
    balloonMaxWidth: 300,
    balloonOffset: [120,100],
    hideIconOnBalloonOpen: false

  }),

  myPlacemark2 = new ymaps.Placemark([43.316545, 45.677634], {
    hintContent: 'Аптека 2',
    balloonContent: '<div class="popover"><p>Телефон: <a href="tel:+78005004671" class="tel">+7 (800) 500-46-71</a></p>' +
                    '<p>Адрес: г. Грозный, ул. Субры Кишиевой (бывшая ул. Киевская), 9</p>' + 
                    '<p>E-mail: <a href="mailto:info@cmo-95.ru">info@cmo-95.ru</a></p></div>'
  }, {
    iconLayout: 'default#image',
    iconImageHref: '/local/templates/cmo95/i/geo.svg',
    iconImageSize: [27, 40],
    iconImageOffset: [-13, -38],
    balloonMaxWidth: 300,
    balloonOffset: [120,100],
    hideIconOnBalloonOpen: false
  }),

  myPlacemark3 = new ymaps.Placemark([43.310246, 45.690337], {
    hintContent: 'Аптека 3',
    balloonContent: '<div class="popover"><p>Телефон: <a href="tel:+78005004671" class="tel">+7 (800) 500-46-71</a></p>' +
                    '<p>Адрес: г. Грозный, ул. Субры Кишиевой (бывшая ул. Киевская), 9</p>' + 
                    '<p>E-mail: <a href="mailto:info@cmo-95.ru">info@cmo-95.ru</a></p></div>'
  }, {
    iconLayout: 'default#image',
    iconImageHref: '/local/templates/cmo95/i/geo.svg',
    iconImageSize: [27, 40],
    iconImageOffset: [-13, -38],
    balloonMaxWidth: 300,
    balloonOffset: [120,100],
    hideIconOnBalloonOpen: false
  });

  myMap.geoObjects
  .add(myPlacemark1)
  .add(myPlacemark2)
  .add(myPlacemark3);

  myMap.behaviors.disable('scrollZoom');
});