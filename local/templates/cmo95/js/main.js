jQuery(document).ready(function($){
  var $header = $('[data-header]');
  var $contacts = $('[data-contacts]');
  var $contactsToggle = $('[data-contacts-toggle]');
  var $nav = $('[data-nav]');
  var $navToggle = $('[data-nav-toggle]');
  var $subnav = $('[data-subnav]');
  var $subnavToggle = $('[data-subnav-toggle]');
  var activeClass = 'active';

  /* Header */
  $(window).on('load scroll resize orientationchange', function() {
    if ($(window).width() >= 1024){
      var bodySwitchClass = 'fixed-header';
      var headerSwitchClass = 'header_scrolled';


      if ($(window).scrollTop() > 0) {
        $('body').addClass(bodySwitchClass);
        $header.addClass(headerSwitchClass);
        $contacts.hide();
      } else {
        $('body').removeClass(bodySwitchClass);
        $header.removeClass(headerSwitchClass);
        $contacts.show();
        $contactsToggle.removeClass(activeClass);
      }
    }
  });

  /* Nav */
  $navToggle.click(function(){
    var $this = $(this);
 
    if ($this.hasClass(activeClass)) {
      $nav.slideUp();
      $this.removeClass(activeClass);
    } else {
      $contacts.hide();
      $nav.slideDown();
      $this.siblings().removeClass(activeClass);
      $this.addClass(activeClass);
    }
  });

  $subnavToggle.click(function(e) {
    e.preventDefault();

    var $this = $(this);

    if ($this.next().hasClass(activeClass)) {
      $this.next().removeClass(activeClass).slideUp(350);
    } else {
      $this.parent().parent().find($subnav).removeClass(activeClass).slideUp(350);
      $this.next().toggleClass(activeClass).slideToggle(350);
    }
  });

  /* Contacts */
  $contactsToggle.click(function(e){
    e.preventDefault();

    if ($(this).hasClass(activeClass)) {
      $contacts.slideUp();
      $(this).removeClass(activeClass);
    } else {
      if ($(window).width() < 1024){
        $nav.hide();
      }
      $contacts.slideDown().css('display', 'flex');
      $(this).siblings().removeClass(activeClass);
      $(this).addClass(activeClass);
      console.log(activeClass);
    }
  });

  /* Panel */
  var $panels = $('[data-panels]');
  var $panelToggle = $('[data-panel-toggle]');
  $panelToggle.click(function(e) {
    e.preventDefault();

    var $this = $(this);

    if ($this.parent().hasClass(activeClass)) {
      $this.parent().find('[data-panel-content]').slideToggle(350);
      $this.parent().removeClass(activeClass);
    } else {
      $this.closest($panels).find('[data-panel-content]').slideUp(350);
      $this.parent().siblings().removeClass(activeClass);
      $this.parent().find('[data-panel-content]').slideDown(350);
      $this.parent().addClass(activeClass);
    }
  });

  /* Feedback form toggle */
  $('[data-feedback-toggle]').click(function(e){
    //e.preventDefault();
    var $feedbackForm = $('[data-feedback-form]');
    $feedbackForm.parent().addClass(activeClass);
    $feedbackForm.slideDown(350);
    $(this).removeAttr('data-feedback-toggle');
  });

  /* Sliders */
  $('[data-hero-slider]').slick({
    infinite: true,
    autoplay: true,
    accessibility: true,
    draggable: true,
    arrows: false,
    dots: true,
    fade:true
  });

  $('[data-brands-slider]').slick({
    infinite: true,
    arrows: true,
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [
      {
        breakpoint: 990,
        settings: {
          infinite: true,
          arrows:true,
          slidesToShow: 4,
          slidesToScroll: 4,
        }
      },
      {
        breakpoint: 768,
        settings: {
          infinite: true,
          arrows:true,
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 576,
        settings: {
          infinite: true,
          arrows:true,
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 480,
        settings: {
          infinite: true,
          arrows:true,
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
    ]
  });

  $(window).on('orientationchange resize', function() {
    $('[data-hero-slider]').slick('resize');
    $('[data-brands-slider]').slick('resize');
  });

  $('body').magnificPopup({
    delegate: 'a[data-gallery]',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0,1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
      titleSrc: function(item) {
        return item.el.attr('title');
      }
    }
  });

  $('[data-reply-form-toggle]').on('click', function(event) {
    event.preventDefault();
    $.magnificPopup.open({
      items: {
        src: $('#reply-form'),
        focus: '#name',
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        callbacks: {
          beforeOpen: function () {
            if ($(window).width() < 700) {
              this.st.focus = false;
            } else {
              this.st.focus = '#name';
            }
          }
        }
      }
    });
  });
});