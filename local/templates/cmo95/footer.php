<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

  </main><!-- /.content -->
</div>
<footer class="footer">
	<?$APPLICATION->IncludeComponent(
		"bitrix:menu",
		"menu_footer",
		Array(
			"ALLOW_MULTI_SELECT" => "N",
			"CHILD_MENU_TYPE" => "left",
			"DELAY" => "N",
			"MAX_LEVEL" => "1",
			"MENU_CACHE_GET_VARS" => array(""),
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"ROOT_MENU_TYPE" => "top",
			"USE_EXT" => "N"
		)
	);?>
  <div class="footer__bottom">
    <div class="container">
      <div class="row">
        <div class="footer__copy col-xs-12 col-sm-6">
          <div class="footer__copy-text">2018 © МУП «Центр муниципального заказа г. Грозного»</div>
        </div><!-- /.col -->

        <div class="footer__pixel col-xs-12 col-sm-6">
          <img src="<?=SITE_TEMPLATE_PATH?>/i/logo-pixel.png" alt="«Пиксель Плюс»" class="footer__pixel-img">
          <span class="footer__pixel-text">Создание сайта — <br>компания «Пиксель Плюс»</span>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </div>
</footer><!-- /.footer -->

<?if(IS_JOBS_PAGE):?>
	<?$APPLICATION->IncludeComponent(
		"k818:k818.feedback.jobs",
		"feedback",
		Array(
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"COMPONENT_TEMPLATE" => "feedback",
			"EMAIL_TO" => "kors.vv@ya.ru",
			"EVENT_MESSAGE_ID" => array(0=>"7",),
			"OK_TEXT" => "Спасибо, ваше сообщение принято.",
			"OK_TEXT2" => "",
			"OK_TEXT3" => "",
			"REQUIRED_FIELDS" => array(),
			"USE_CAPTCHA" => "N"
		)
	);?>
<?endif;?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/slick.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/map.js"></script>
	
</body>
</html>