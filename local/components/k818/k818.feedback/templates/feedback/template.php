<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<?
if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?>
<!--		<div class="mf-ok-text">-->
<!--			--><?//=$arResult["OK_MESSAGE"]?>
<!--		</div>-->
	<?
}
?>

<form action="<?=POST_FORM_ACTION_URI?>" method="POST" class="feedback__form form">
	<?=bitrix_sessid_post()?>
	<?if(strlen($arResult["OK_MESSAGE"]) > 0):?>
		<div class="call-form__header col-xs-12 col-md-6 col-lg-4">
			<h2 class="call-form__title">
				<?=$arResult["OK_MESSAGE"]?>
			</h2>
		</div>
	<?else:?>
		<p><strong>Если у вас есть вопрос, вы всегда можете задать его нам через форму обратной связи</strong></p>
		<div class="feedback__form-body hidden" data-feedback-form>
			<div class="form__group">
				<label class="form__label hidden" for="name">Ваше имя</label>
				<input type="text" class="form__control" name="user_name" placeholder="Ваше имя" required="">
			</div>
			<div class="form__group">
				<label class="form__label hidden" for="email">Ваш E-mail</label>
				<input type="text" class="form__control" name="user_email" placeholder="Ваш E-mail" required="">
			</div>
			<div class="form__group">
				<label class="form__label hidden" for="message">Ваш вопрос</label>
				<textarea rows="5" class="form__control" name="MESSAGE" placeholder="Ваш вопрос" required=""></textarea>
			</div>

		</div>
		<div class="form__group form__group_submit">
				<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
				<input type="submit" name="submit" class="button button_primary" value="Задать вопрос" data-feedback-toggle>
		</div>

	<?endif;?>
</form>
