<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<?
if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}

if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?>
<!--		<div class="mf-ok-text">-->
<!--			--><?//=$arResult["OK_MESSAGE"]?>
<!--		</div>-->
	<?
}
?>
<form action="<?=POST_FORM_ACTION_URI?>" method="POST" id="reply-form" class="modal mfp-hide">
	<?=bitrix_sessid_post()?>

	<? if(strlen($arResult["OK_MESSAGE"]) > 0): ?>
		<h3 class="call-form-sm__title ok_message"><?=$arResult["OK_MESSAGE"]?></h3>
	<?else:?>

		<h2 class="modal__title">Откликнуться на вакансию</h2>
		<p class="modal__text">Напишите о себе</p>

		<div class="form__group">
			<label for="name" class="form__label">Ваше имя</label>
			<input id="name" name="user_name" placeholder="Ваше имя" required="" type="text" class="form__control" value="<?=$arResult["AUTHOR_NAME"]?>">
		</div>
		<div class="form__group">
			<label for="phone" class="form__label">Телефон для связи</label>
			<input id="phone" name="user_phone" placeholder="Телефон для связи" required="" type="text" class="form__control" value="<?=$arResult["AUTHOR_PHONE"]?>">
		</div>
		<div class="form__group">
			<label for="message" class="form__label">Коротко о себе</label>
			<textarea id="message" name="MESSAGE" placeholder="Коротко о себе" required="" rows="8" class="form__control"><?=$arResult["MESSAGE"]?></textarea>
		</div>

		<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
		<div class="form__group form__group_submit">
			<input type="submit" name="submit" class="button button_primary button_block" value="Отправить">
		</div>
	<?endif;?>

</form >
